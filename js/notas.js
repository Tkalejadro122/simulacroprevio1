let codigo;
let notas;
let nombre;
let materia;
let promedio;
let aprobados;
let reprobados;

var calificaciones = [];
var arreglado = [];
let descripcion = [];
let descripciones = ["a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"];

function obtenerDatos() {
    let url = new URLSearchParams(location.search);
    codigo = url.get('codigo');
    notas = Number(url.get('numero'));
    fetchAlumno(codigo);
}

function fetchAlumno(id) {
    fetch(`https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json`)
        .then(res => res.json())
        //.then(data => console.log(data.estudiantes[1]))
        .then(data => {
            materia = data.nombreMateria;
            conseguirAlumno(data, id);
            document.getElementById("tablaPersona").innerHTML = crearTabla();
            copiarCalifiaciones();
            arreglado.sort();
            describir(notas);
            catalogar(data);
            console.log(descripciones);
            calcularPromedio(notas);
            imprimirInfo();
        })
}

function conseguirAlumno(arreglo, codigo) {
    let conseguido = false;
    for (let i = 0; i < arreglo.estudiantes.length; i++) {
        if (arreglo.estudiantes[i].codigo == codigo) {
            nombre = arreglo.estudiantes[i].nombre;
            console.log(arreglo.estudiantes[i]);
            conseguido = true;
            agregarCalificaciones(arreglo, i);
        }
    }
    if (conseguido == false) {
        alert("Código equivocado");
    }
}

function agregarCalificaciones(arreglo, index) {
    for (let i = 0; i < 15; i++) {
        calificaciones.push(arreglo.estudiantes[index].notas[i].valor);
    }
}

function copiarCalifiaciones() {
    for (let i = 0; i < 15; i++) {
        arreglado.push(calificaciones[i]);
    }
}



function describir(limite) {
    let continuar = true;

    for (let i = 0; i < limite; i++) {
        continuar = true;
        for (let j = 0; j < 15 && continuar; j++) {
            if (calificaciones[j] == arreglado[i] && descripciones[j] == "a") {
                descripciones[j] = "Nota eliminada";
                continuar = false;
            }
        }
    }

    console.log(descripciones);

    for (let i = 0; i < 15; i++) {
        if (descripciones[i] != "Nota eliminada") {
            if (calificaciones[i] < 3.0) {
                descripciones[i] = "Nota reprobada";
            }
            else {
                descripciones[i] = "Nota aprobada";
            }
        }
    }

    let a = 0;
    let r = 0;

    for(let i = 0; i < 15; i++){
        if (descripciones[i] == "Nota aprobada"){
            a++;
        }
        else if(descripciones[i] == "Nota reprobada"){
            r++;
        }
    }

    aprobados = a;
    reprobados = r;
}

function catalogar(info) {
    for (let i = 0; i < 15; i++) {
        descripcion.push(info.descripcion[i].descripcion);
    }
}

function calcularPromedio(limite) {
    let x = 0;
    for (let i = 0; i < 15; i++) {
        if (descripciones[i] != "Nota eliminada") {
            x += calificaciones[i];
        }
    }
    x /= (15 - limite);
    promedio = x;
}



function imprimirInfo() {
    console.log(codigo);
    console.log(notas);
    console.log(nombre);
    console.log(materia);
    console.log(calificaciones);
    console.log(arreglado);
    console.log(promedio.toFixed(1));
    console.log(descripcion);
}

function crearTabla() {
    let stringTabla = `<tr><td>Nombre: </td><td>${nombre}</td></tr>`;
    let fila2 = `<tr><td>Código: </td><td>${codigo}</td></tr>`;
    let fila3 = `<tr><td>Materia: </td><td>${materia}</td></tr>`;

    stringTabla += fila2;
    stringTabla += fila3;

    return stringTabla;
}

obtenerDatos();

google.charts.load('current', { 'packages': ['table'] });
google.charts.setOnLoadCallback(drawTable);

function drawTable() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descripción');
    data.addColumn('number', 'Promedio');
    data.addColumn('string', 'Observacion');
    data.addRows(16);

    for (let i = 0; i < 15; i++) {
        data.setCell(i, 0, descripcion[i]);
        data.setCell(i, 1, calificaciones[i]);
        data.setCell(i, 2, descripciones[i]);
    }

    data.setCell(15, 0, "Nota definitiva");
    data.setCell(15, 1, promedio.toFixed(1));
    data.setCell(15, 2, "Nota definitiva");

    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, { showRowNumber: false, width: '100%', height: '100%'});
}

google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Descripción', 'Cantidad'],
          ['Aprobado',     aprobados],
          ['Reprobado',      reprobados]
        ]);


        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data);
      }
